FROM php:7.2-apache

RUN apt-get update && \
    apt-get install -y libfreetype6-dev libxml2-dev libaio-dev unzip jq

# Install PHP required extensions
# -- GD
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ && \
    docker-php-ext-install -j$(nproc) gd \        
        pdo_mysql \
        mysqli \
        exif \
        soap && \
    apt-get clean y

# Install Composer
RUN curl -k -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Enable apache module rewrite
RUN cd /etc/apache2/mods-enabled && ln -s ../mods-available/rewrite.load